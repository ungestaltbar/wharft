<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en" itemscope itemtype="http://schema.org/Product"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <!-- Use the .htaccess and remove these lines to avoid edge case issues.
                 More info: h5bp.com/b/378 -->
        <title><?php wp_title( '|', true, 'right' ); ?></title>

        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="humans.txt">

        <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>

    <nav class="top-bar" data-topbar>
        <ul class="title-area">
            <li class="name">
                <h1><a href="#">The Wharft</a></h1>
            </li>
            <li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
        </ul>

        <section class="top-bar-section">


            <!-- Right Nav Section -->
           <?php $defaults = array(
            'theme_location'  => 'primary',
            'menu'            => '',
            'container'       => '',
            'container_class' => '',
            'container_id'    => '',
            'menu_class'      => 'button-group',
            'menu_id'         => '',
            'echo'            => true,
            'fallback_cb'     => 'wp_nav_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul class="left %2$s" role="navigation">%3$s</ul>',
            'depth'           => 0,
            'walker'          => new p2p_foundation_walker()
            );

            wp_nav_menu( $defaults ); ?>


        </section>
    </nav>
