module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        compass: {// Task
            dev: {
                options: {
                    config: 'assets/config.rb',
                    trace: false,
                    outputStyle: 'expanded'
                }
            },
            dist: {
                options: {
                    config: 'assets/config.rb',
                    trace: false,
                    outputStyle: 'compressed'
                }
            }
        },
        watch: {
            options: {
                livereload: true
            },
            js: {
                files: ['assets/js/plugins.js','assets/js/scripts.js'],
                tasks: ['concat']
            },
            sass: {
                options: {
                    livereload: false
                },
                files: ['assets/**/*.scss'],
                tasks: ['compass:dev']
            },
            css: {
                files: ['assets/*.css'],
                tasks: []
            }
        },
        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: ['assets/vendor/bower_components/foundation/js/vendor/modernizr.js',
                    'assets/vendor/bower_components/foundation/js/vendor/fastclick.js',
                    'assets/vendor/bower_components/foundation/js/foundation/foundation.js',
                    'assets/js/plugins.js',
                    'assets/js/scripts.js'
                ],
                dest: 'assets/js/dist.js'
            },
            foundation: {
                src: ['assets/vendor/bower_components/foundation/js/foundation/**/*.js'],
                dest: 'assets/js/foundation.components.js'
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-rsync-2');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-shell');

    // Default task(s).
    grunt.registerTask('default', ['compass:dev', 'concat']);
    grunt.registerTask('assets', ['shell']);
    grunt.registerTask('dist', ['compass:dist', 'concat']);
};