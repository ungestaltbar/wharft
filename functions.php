<?php

//
require_once 'Autoloader.php';
require_once 'includes/PostCustomForm.php';

$Restrap = new Reframe\Theme();

$Restrap->Scripts
    ->addFooterScript( 'main-scripts', 'assets/js/dist.js')
    ->addFooterScript( 'foundation', 'assets/js/foundation.components.js', array('jquery', 'underscore', 'main-scripts') );


$Restrap->Styles
    ->addStyle( 'foundation', 'assets/css/sass-foundation.css' )
    ->addStyle( 'main-min', 'assets/css/main.css' );

$Restrap->ThemeSupport
    ->add( 'menus' )
    ->add( 'post-thumbnails', array( 'post' ) );

$Restrap->Menus
    ->add( 'primary', 'Hauptnavigation' );

$Restrap->Areas
    ->add( array(
        'id' => 'header-primary',
        'name' => 'Header',
        'postTypes' => array( 'page' ),
        'layouts' => array( 'default' ),
        'context' => 'top',
        'limit' => 2,
        'order' => 10
    ) )
    ->add( array(
        'id' => 'content-primary',
        'name' => 'Content',
        'postTypes' => array( 'page' ),
        'layouts' => array( 'default', '2-columns' ),
        'context' => 'normal',
        'order' => 10
    ) )
    ->add( array(
        'id' => 'content-primary-2',
        'name' => 'Content',
        'postTypes' => array( 'page' ),
        'layouts' => array( 'default', '2-columns', 'mixed-columns' ),
        'context' => 'normal',
        'order' => 20
    ) )
    ->add( array(
        'id' => 'content-secondary',
        'name' => 'Secondary',
        'postTypes' => array( 'fake' ),
        'layouts' => array( 'default', '2-columns','3-columns' ),
        'context' => 'side',
        'order' => 20
    ) );


$Restrap->Kontentblocks
    ->addPath( 'modules' );

if ( !function_exists( 'kb_register_area_template' ) )
    return;

kb_register_area_template( array(
    'id' => 'default',
    'label' => 'default',
    'layout' => false
) );

kb_register_area_template( array(
    'id' => '2-columns',
    'label' => 'Two Columns',
    'cycle' => false,
    'layout' => array(
        array(
            'classes' => 'large-3 fl',
            'columns' => 2
        ),
        array(
            'classes' => 'large-3 fl ',
            'columns' => 2
        ),
        array(
            'classes' => 'large-3 fl ',
            'columns' => 2
        ),
        array(
            'classes' => 'large-3 fl last',
            'columns' => 2
        ),
        array(
            'classes' => 'large-12 fl last',
            'columns' => 2
        )
    )
) );

kb_register_area_template( array(
    'id' => 'mixed-columns',
    'label' => 'Mixed',
    'cycle' => true,
    'layout' => array(
        array(
            'classes' => 'large-9 small-12 medium-12 fl',
            'columns' => 2
        ),
        array(
            'classes' => 'large-3 small-12 medium-12 fl last ',
            'columns' => 2
        ),
        array(
            'classes' => 'large-3 small-12 medium-12 fl ',
            'columns' => 2
        ),
        array(
            'classes' => 'large-9 small-12 medium-12 fl last',
            'columns' => 2
        ),
        array(
            'classes' => 'large-12 small-12 medium-12 fl last',
            'columns' => 2
        )
    )
) );

kb_register_area_template( array(
    'id' => '3-columns',
    'label' => 'Three Columns',
    'templateClass' => array('inner', 'container'),
    'layout' => array(
        array(
            'classes' => 'one-third',
            'columns' => 3
        )
    ),
    'last-item' => 3,
    'cycle' => true
) );


// Add "has-dropdown" CSS class to navigation menu items that have children in a submenu.
function nav_menu_item_parent_classing( $classes, $item )
{
    global $wpdb;

    $has_children = $wpdb -> get_var( "SELECT COUNT(meta_id) FROM {$wpdb->prefix}postmeta WHERE meta_key='_menu_item_menu_item_parent' AND meta_value='" . $item->ID . "'" );

    if ( $has_children > 0 )
    {
        array_push( $classes, "has-dropdown" );
    }

    return $classes;
}

add_filter( "nav_menu_css_class", "nav_menu_item_parent_classing", 10, 2 );

//Deletes empty classes and changes the sub menu class name
function change_submenu_class($menu) {
    $menu = preg_replace('/ class="sub-menu"/',' class="dropdown"',$menu);
    return $menu;
}
add_filter ('wp_nav_menu','change_submenu_class');


//Use the active class of the ZURB Foundation for the current menu item. (From: https://github.com/milohuang/reverie/blob/master/functions.php)
function required_active_nav_class( $classes, $item ) {
    if ( $item->current == 1 || $item->current_item_ancestor == true ) {
        $classes[] = 'active';
    }
    return $classes;
}
add_filter( 'nav_menu_css_class', 'required_active_nav_class', 10, 2 );

// Search Form
function joints_wpsearch($form) {
    $form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
	<label class="screen-reader-text" for="s">' . __('Search for:', 'jointstheme') . '</label>
	<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="'.esc_attr__('Search the Site...','jointstheme').'" />
	<input type="submit" id="searchsubmit" class="button" value="'. esc_attr__('Search') .'" />
	</form>';
    return $form;
} // don't remove this bracket!



class p2p_foundation_walker extends Walker_Nav_Menu{

    function start_el(&$output, $item, $depth = 0, $args = Array(), $id = 0){

        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $current_indicators = array('current-menu-item', 'current-menu-parent', 'current_page_item', 'current_page_parent');

        $newClasses = array('button');

        foreach($classes as $el){
            //add button class and check if it's indicating the current page.
            if (in_array($el, $current_indicators)){
                array_push($newClasses, $el);
            }
        }

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $newClasses), $item ) );
        if($class_names!='') $class_names = ' class="'. esc_attr( $class_names ) . '"';


        $output .= $indent . '<li>';

        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';


        $item_output = $args->before;
        $item_output .= '<a'. $attributes . $class_names .'>';
        $item_output .= $args->link_before .apply_filters( 'the_title', $item->title, $item->ID );
        $item_output .= '</a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}

