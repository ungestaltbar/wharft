<?php

namespace Reframe;

// register SPL Autoloader
spl_autoload_register( 'Reframe\Autoloader::ClassLoader' );

class Autoloader
{

    public static function ClassLoader( $className )
    {
        $className = $class_name = str_replace( '\\', '/', $className );
//        echo dirname(__FILE__) . DIRECTORY_SEPARATOR . $className . '.php';
        if ( file_exists( dirname(__FILE__) . DIRECTORY_SEPARATOR . $className . '.php' ) ) {
            include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . $className . '.php';
        }

    }

}
