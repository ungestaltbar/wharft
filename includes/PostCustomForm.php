<?php

use Kontentblocks\Panels\CustomModulePanel;

class PostCustomForm extends \Kontentblocks\Modules\StaticModule{

    public function render($data)
    {
        $TPL = new \Kontentblocks\Templating\ModuleTemplate($this, 'test.twig', array());
        $TPL->setPath(get_stylesheet_directory() . '/module-templates/');
        return $TPL->render();
    }

    function fields()
    {
        $groupA = $this->Fields->addGroup('group-a', array('label' => 'Testdrive'));
        $groupA
            ->addField('image', 'whoo', array(
                'label' => 'Page Header',
                'description' => 'Fullscreen background image',
                'hideMeta' => true,
                'previewSize' => array(300, 150),
                'returnObj' => 'Image'
            ))
            ->addField('file', 'file', array(
                'label' => 'File',
                'description' => 'Download article pdf',
                'hideMeta' => true
            ));
    }
}

add_action('init', function(){

    \Kontentblocks\Panels\PanelRegistry::getInstance()->add('testid', array(
        'baseId' => 'testid',
        'postTypes' => array('post'),
        'moduleClass' => 'PostCustomForm'
    ));

//    new CustomModulePanel(array(
//        'baseId' => 'test',
//        'postTypes' => array('post')
//    ), 'PostCustomForm');
});