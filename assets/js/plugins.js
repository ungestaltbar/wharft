/*! matchMedia() polyfill - Test a CSS media type/query in JS. Authors & copyright (c) 2012: Scott Jehl, Paul Irish, Nicholas Zakas. Dual MIT/BSD license */
/*! NOTE: If you're already including a window.matchMedia polyfill via Modernizr or otherwise, you don't need this part */
window.matchMedia=window.matchMedia||function(a){"use strict";var c,d=a.documentElement,e=d.firstElementChild||d.firstChild,f=a.createElement("body"),g=a.createElement("div");return g.id="mq-test-1",g.style.cssText="position:absolute;top:-100em",f.style.background="none",f.appendChild(g),function(a){return g.innerHTML='&shy;<style media="'+a+'"> #mq-test-1 { width: 42px; }</style>',d.insertBefore(f,e),c=42===g.offsetWidth,d.removeChild(f),{matches:c,media:a}}}(document);

/*! Respond.js v1.3.0: min/max-width media query polyfill. (c) Scott Jehl. MIT/GPLv2 Lic. j.mp/respondjs  */
(function(a){"use strict";function x(){u(!0)}var b={};if(a.respond=b,b.update=function(){},b.mediaQueriesSupported=a.matchMedia&&a.matchMedia("only all").matches,!b.mediaQueriesSupported){var q,r,t,c=a.document,d=c.documentElement,e=[],f=[],g=[],h={},i=30,j=c.getElementsByTagName("head")[0]||d,k=c.getElementsByTagName("base")[0],l=j.getElementsByTagName("link"),m=[],n=function(){for(var b=0;l.length>b;b++){var c=l[b],d=c.href,e=c.media,f=c.rel&&"stylesheet"===c.rel.toLowerCase();d&&f&&!h[d]&&(c.styleSheet&&c.styleSheet.rawCssText?(p(c.styleSheet.rawCssText,d,e),h[d]=!0):(!/^([a-zA-Z:]*\/\/)/.test(d)&&!k||d.replace(RegExp.$1,"").split("/")[0]===a.location.host)&&m.push({href:d,media:e}))}o()},o=function(){if(m.length){var b=m.shift();v(b.href,function(c){p(c,b.href,b.media),h[b.href]=!0,a.setTimeout(function(){o()},0)})}},p=function(a,b,c){var d=a.match(/@media[^\{]+\{([^\{\}]*\{[^\}\{]*\})+/gi),g=d&&d.length||0;b=b.substring(0,b.lastIndexOf("/"));var h=function(a){return a.replace(/(url\()['"]?([^\/\)'"][^:\)'"]+)['"]?(\))/g,"$1"+b+"$2$3")},i=!g&&c;b.length&&(b+="/"),i&&(g=1);for(var j=0;g>j;j++){var k,l,m,n;i?(k=c,f.push(h(a))):(k=d[j].match(/@media *([^\{]+)\{([\S\s]+?)$/)&&RegExp.$1,f.push(RegExp.$2&&h(RegExp.$2))),m=k.split(","),n=m.length;for(var o=0;n>o;o++)l=m[o],e.push({media:l.split("(")[0].match(/(only\s+)?([a-zA-Z]+)\s?/)&&RegExp.$2||"all",rules:f.length-1,hasquery:l.indexOf("(")>-1,minw:l.match(/\(\s*min\-width\s*:\s*(\s*[0-9\.]+)(px|em)\s*\)/)&&parseFloat(RegExp.$1)+(RegExp.$2||""),maxw:l.match(/\(\s*max\-width\s*:\s*(\s*[0-9\.]+)(px|em)\s*\)/)&&parseFloat(RegExp.$1)+(RegExp.$2||"")})}u()},s=function(){var a,b=c.createElement("div"),e=c.body,f=!1;return b.style.cssText="position:absolute;font-size:1em;width:1em",e||(e=f=c.createElement("body"),e.style.background="none"),e.appendChild(b),d.insertBefore(e,d.firstChild),a=b.offsetWidth,f?d.removeChild(e):e.removeChild(b),a=t=parseFloat(a)},u=function(b){var h="clientWidth",k=d[h],m="CSS1Compat"===c.compatMode&&k||c.body[h]||k,n={},o=l[l.length-1],p=(new Date).getTime();if(b&&q&&i>p-q)return a.clearTimeout(r),r=a.setTimeout(u,i),void 0;q=p;for(var v in e)if(e.hasOwnProperty(v)){var w=e[v],x=w.minw,y=w.maxw,z=null===x,A=null===y,B="em";x&&(x=parseFloat(x)*(x.indexOf(B)>-1?t||s():1)),y&&(y=parseFloat(y)*(y.indexOf(B)>-1?t||s():1)),w.hasquery&&(z&&A||!(z||m>=x)||!(A||y>=m))||(n[w.media]||(n[w.media]=[]),n[w.media].push(f[w.rules]))}for(var C in g)g.hasOwnProperty(C)&&g[C]&&g[C].parentNode===j&&j.removeChild(g[C]);for(var D in n)if(n.hasOwnProperty(D)){var E=c.createElement("style"),F=n[D].join("\n");E.type="text/css",E.media=D,j.insertBefore(E,o.nextSibling),E.styleSheet?E.styleSheet.cssText=F:E.appendChild(c.createTextNode(F)),g.push(E)}},v=function(a,b){var c=w();c&&(c.open("GET",a,!0),c.onreadystatechange=function(){4!==c.readyState||200!==c.status&&304!==c.status||b(c.responseText)},4!==c.readyState&&c.send(null))},w=function(){var b=!1;try{b=new a.XMLHttpRequest}catch(c){b=new a.ActiveXObject("Microsoft.XMLHTTP")}return function(){return b}}();n(),b.update=n,a.addEventListener?a.addEventListener("resize",x,!1):a.attachEvent&&a.attachEvent("onresize",x)}})(this);

/*

 Holder - 2.0 - client side image placeholders
 (c) 2012-2013 Ivan Malopinsky / http://imsky.co

 Provided under the Apache 2.0 License: http://www.apache.org/licenses/LICENSE-2.0
 Commercial use requires attribution.

 */

var Holder = Holder || {};
(function (app, win) {

    var preempted = false,
        fallback = false,
        canvas = document.createElement('canvas');

//getElementsByClassName polyfill
    document.getElementsByClassName||(document.getElementsByClassName=function(e){var t=document,n,r,i,s=[];if(t.querySelectorAll)return t.querySelectorAll("."+e);if(t.evaluate){r=".//*[contains(concat(' ', @class, ' '), ' "+e+" ')]",n=t.evaluate(r,t,null,0,null);while(i=n.iterateNext())s.push(i)}else{n=t.getElementsByTagName("*"),r=new RegExp("(^|\\s)"+e+"(\\s|$)");for(i=0;i<n.length;i++)r.test(n[i].className)&&s.push(n[i])}return s})

//getComputedStyle polyfill
    window.getComputedStyle||(window.getComputedStyle=function(e,t){return this.el=e,this.getPropertyValue=function(t){var n=/(\-([a-z]){1})/g;return t=="float"&&(t="styleFloat"),n.test(t)&&(t=t.replace(n,function(){return arguments[2].toUpperCase()})),e.currentStyle[t]?e.currentStyle[t]:null},this})

//http://javascript.nwbox.com/ContentLoaded by Diego Perini with modifications
    function contentLoaded(n,t){var l="complete",s="readystatechange",u=!1,h=u,c=!0,i=n.document,a=i.documentElement,e=i.addEventListener?"addEventListener":"attachEvent",v=i.addEventListener?"removeEventListener":"detachEvent",f=i.addEventListener?"":"on",r=function(e){(e.type!=s||i.readyState==l)&&((e.type=="load"?n:i)[v](f+e.type,r,u),!h&&(h=!0)&&t.call(n,null))},o=function(){try{a.doScroll("left")}catch(n){setTimeout(o,50);return}r("poll")};if(i.readyState==l)t.call(n,"lazy");else{if(i.createEventObject&&a.doScroll){try{c=!n.frameElement}catch(y){}c&&o()}i[e](f+"DOMContentLoaded",r,u),i[e](f+s,r,u),n[e](f+"load",r,u)}};

//https://gist.github.com/991057 by Jed Schmidt with modifications
    function selector(a){
        a=a.match(/^(\W)?(.*)/);var b=document["getElement"+(a[1]?a[1]=="#"?"ById":"sByClassName":"sByTagName")](a[2]);
        var ret=[];	b!=null&&(b.length?ret=b:b.length==0?ret=b:ret=[b]);	return ret;
    }

//shallow object property extend
    function extend(a,b){var c={};for(var d in a)c[d]=a[d];for(var e in b)c[e]=b[e];return c}

//hasOwnProperty polyfill
    if (!Object.prototype.hasOwnProperty)
        Object.prototype.hasOwnProperty = function(prop) {
            var proto = this.__proto__ || this.constructor.prototype;
            return (prop in this) && (!(prop in proto) || proto[prop] !== this[prop]);
        }

    function text_size(width, height, template) {
        height = parseInt(height,10);
        width = parseInt(width,10);
        var bigSide = Math.max(height, width)
        var smallSide = Math.min(height, width)
        var scale = 1 / 12;
        var newHeight = Math.min(smallSide * 0.75, 0.75 * bigSide * scale);
        return {
            height: Math.round(Math.max(template.size, newHeight))
        }
    }

    function draw(ctx, dimensions, template, ratio) {
        var ts = text_size(dimensions.width, dimensions.height, template);
        var text_height = ts.height;
        var width = dimensions.width * ratio,
            height = dimensions.height * ratio;
        var font = template.font ? template.font : "sans-serif";
        canvas.width = width;
        canvas.height = height;
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        ctx.fillStyle = template.background;
        ctx.fillRect(0, 0, width, height);
        ctx.fillStyle = template.foreground;
        ctx.font = "bold " + text_height + "px " + font;
        var text = template.text ? template.text : (Math.floor(dimensions.width) + "x" + Math.floor(dimensions.height));
        var text_width = ctx.measureText(text).width;
        if (text_width / width >= 0.75) {
            text_height = Math.floor(text_height * 0.75 * (width/text_width));
        }
        //Resetting font size if necessary
        ctx.font = "bold " + (text_height * ratio) + "px " + font;
        ctx.fillText(text, (width / 2), (height / 2), width);
        return canvas.toDataURL("image/png");
    }

    function render(mode, el, holder, src) {
        var dimensions = holder.dimensions,
            theme = holder.theme,
            text = holder.text ? decodeURIComponent(holder.text) : holder.text;
        var dimensions_caption = dimensions.width + "x" + dimensions.height;
        theme = (text ? extend(theme, {
            text: text
        }) : theme);
        theme = (holder.font ? extend(theme, {
            font: holder.font
        }) : theme);
        if (mode == "image") {
            el.setAttribute("data-src", src);
            el.setAttribute("alt", text ? text : theme.text ? theme.text + " [" + dimensions_caption + "]" : dimensions_caption);
            if (fallback || !holder.auto) {
                el.style.width = dimensions.width + "px";
                el.style.height = dimensions.height + "px";
            }
            if (fallback) {
                el.style.backgroundColor = theme.background;
            } else {
                el.setAttribute("src", draw(ctx, dimensions, theme, ratio));
            }
        } else if (mode == "background") {
            if (!fallback) {
                el.style.backgroundImage = "url(" + draw(ctx, dimensions, theme, ratio) + ")";
                el.style.backgroundSize = dimensions.width + "px " + dimensions.height + "px";
            }
        } else if (mode == "fluid") {
            el.setAttribute("data-src", src);
            el.setAttribute("alt", text ? text : theme.text ? theme.text + " [" + dimensions_caption + "]" : dimensions_caption);
            if (dimensions.height.substr(-1) == "%") {
                el.style.height = dimensions.height
            } else {
                el.style.height = dimensions.height + "px"
            }
            if (dimensions.width.substr(-1) == "%") {
                el.style.width = dimensions.width
            } else {
                el.style.width = dimensions.width + "px"
            }
            if (el.style.display == "inline" || el.style.display == "") {
                el.style.display = "block";
            }
            if (fallback) {
                el.style.backgroundColor = theme.background;
            } else {
                el.holderData = holder;
                fluid_images.push(el);
                fluid_update(el);
            }
        }
    };

    function fluid_update(element) {
        var images;
        if (element.nodeType == null) {
            images = fluid_images;
        } else {
            images = [element]
        }
        for (i in images) {
            var el = images[i]
            if (el.holderData) {
                var holder = el.holderData;
                el.setAttribute("src", draw(ctx, {
                    height: el.clientHeight,
                    width: el.clientWidth
                }, holder.theme, ratio));
            }
        }
    }

    function parse_flags(flags, options) {

        var ret = {
            theme: settings.themes.gray
        }, render = false;

        for (sl = flags.length, j = 0; j < sl; j++) {
            var flag = flags[j];
            if (app.flags.dimensions.match(flag)) {
                render = true;
                ret.dimensions = app.flags.dimensions.output(flag);
            } else if (app.flags.fluid.match(flag)) {
                render = true;
                ret.dimensions = app.flags.fluid.output(flag);
                ret.fluid = true;
            } else if (app.flags.colors.match(flag)) {
                ret.theme = app.flags.colors.output(flag);
            } else if (options.themes[flag]) {
                //If a theme is specified, it will override custom colors
                ret.theme = options.themes[flag];
            } else if (app.flags.text.match(flag)) {
                ret.text = app.flags.text.output(flag);
            } else if (app.flags.font.match(flag)) {
                ret.font = app.flags.font.output(flag);
            } else if (app.flags.auto.match(flag)) {
                ret.auto = true;
            }
        }

        return render ? ret : false;

    };



    if (!canvas.getContext) {
        fallback = true;
    } else {
        if (canvas.toDataURL("image/png")
            .indexOf("data:image/png") < 0) {
            //Android doesn't support data URI
            fallback = true;
        } else {
            var ctx = canvas.getContext("2d");
        }
    }

    var dpr = 1, bsr = 1;

    if(!fallback){
        dpr = window.devicePixelRatio || 1,
            bsr = ctx.webkitBackingStorePixelRatio || ctx.mozBackingStorePixelRatio || ctx.msBackingStorePixelRatio || ctx.oBackingStorePixelRatio || ctx.backingStorePixelRatio || 1;
    }

    var ratio = dpr / bsr;

    var fluid_images = [];

    var settings = {
        domain: "holder.js",
        images: "img",
        bgnodes: ".holderjs",
        themes: {
            "gray": {
                background: "#eee",
                foreground: "#aaa",
                size: 12
            },
            "social": {
                background: "#3a5a97",
                foreground: "#fff",
                size: 12
            },
            "industrial": {
                background: "#434A52",
                foreground: "#C2F200",
                size: 12
            }
        },
        stylesheet: ".holderjs-fluid {font-size:16px;font-weight:bold;text-align:center;font-family:sans-serif;margin:0}"
    };


    app.flags = {
        dimensions: {
            regex: /^(\d+)x(\d+)$/,
            output: function (val) {
                var exec = this.regex.exec(val);
                return {
                    width: +exec[1],
                    height: +exec[2]
                }
            }
        },
        fluid: {
            regex: /^([0-9%]+)x([0-9%]+)$/,
            output: function (val) {
                var exec = this.regex.exec(val);
                return {
                    width: exec[1],
                    height: exec[2]
                }
            }
        },
        colors: {
            regex: /#([0-9a-f]{3,})\:#([0-9a-f]{3,})/i,
            output: function (val) {
                var exec = this.regex.exec(val);
                return {
                    size: settings.themes.gray.size,
                    foreground: "#" + exec[2],
                    background: "#" + exec[1]
                }
            }
        },
        text: {
            regex: /text\:(.*)/,
            output: function (val) {
                return this.regex.exec(val)[1];
            }
        },
        font: {
            regex: /font\:(.*)/,
            output: function (val) {
                return this.regex.exec(val)[1];
            }
        },
        auto: {
            regex: /^auto$/
        }
    }

    for (var flag in app.flags) {
        if (!app.flags.hasOwnProperty(flag)) continue;
        app.flags[flag].match = function (val) {
            return val.match(this.regex)
        }
    }

    app.add_theme = function (name, theme) {
        name != null && theme != null && (settings.themes[name] = theme);
        return app;
    };

    app.add_image = function (src, el) {
        var node = selector(el);
        if (node.length) {
            for (var i = 0, l = node.length; i < l; i++) {
                var img = document.createElement("img")
                img.setAttribute("data-src", src);
                node[i].appendChild(img);
            }
        }
        return app;
    };

    app.run = function (o) {
        var options = extend(settings, o),
            images = [], imageNodes = [], bgnodes = [];

        if(typeof(options.images) == "string"){
            imageNodes = selector(options.images);
        }
        else if (window.NodeList && options.images instanceof window.NodeList) {
            imageNodes = options.images;
        } else if (window.Node && options.images instanceof window.Node) {
            imageNodes = [options.images];
        }

        if(typeof(options.bgnodes) == "string"){
            bgnodes = selector(options.bgnodes);
        } else if (window.NodeList && options.elements instanceof window.NodeList) {
            bgnodes = options.bgnodes;
        } else if (window.Node && options.bgnodes instanceof window.Node) {
            bgnodes = [options.bgnodes];
        }

        preempted = true;

        for (i = 0, l = imageNodes.length; i < l; i++) images.push(imageNodes[i]);

        var holdercss = document.getElementById("holderjs-style");
        if (!holdercss) {
            holdercss = document.createElement("style");
            holdercss.setAttribute("id", "holderjs-style");
            holdercss.type = "text/css";
            document.getElementsByTagName("head")[0].appendChild(holdercss);
        }

        if (!options.nocss) {
            if (holdercss.styleSheet) {
                holdercss.styleSheet.cssText += options.stylesheet;
            } else {
                holdercss.appendChild(document.createTextNode(options.stylesheet));
            }
        }

        var cssregex = new RegExp(options.domain + "\/(.*?)\"?\\)");

        for (var l = bgnodes.length, i = 0; i < l; i++) {
            var src = window.getComputedStyle(bgnodes[i], null)
                .getPropertyValue("background-image");
            var flags = src.match(cssregex);
            var bgsrc = bgnodes[i].getAttribute("data-background-src");

            if (flags) {
                var holder = parse_flags(flags[1].split("/"), options);
                if (holder) {
                    render("background", bgnodes[i], holder, src);
                }
            }
            else if(bgsrc != null){
                var holder = parse_flags(bgsrc.substr(bgsrc.lastIndexOf(options.domain) + options.domain.length + 1)
                    .split("/"), options);
                if(holder){
                    render("background", bgnodes[i], holder, src);
                }
            }
        }

        for (l = images.length, i = 0; i < l; i++) {

            var attr_src = attr_data_src = src = null;

            try{
                attr_src = images[i].getAttribute("src");
                attr_datasrc = images[i].getAttribute("data-src");
            }catch(e){}

            if (attr_datasrc == null && !! attr_src && attr_src.indexOf(options.domain) >= 0) {
                src = attr_src;
            } else if ( !! attr_datasrc && attr_datasrc.indexOf(options.domain) >= 0) {
                src = attr_datasrc;
            }

            if (src) {
                var holder = parse_flags(src.substr(src.lastIndexOf(options.domain) + options.domain.length + 1)
                    .split("/"), options);
                if (holder) {
                    if (holder.fluid) {
                        render("fluid", images[i], holder, src)
                    } else {
                        render("image", images[i], holder, src);
                    }
                }
            }
        }
        return app;
    };

    contentLoaded(win, function () {
        if (window.addEventListener) {
            window.addEventListener("resize", fluid_update, false);
            window.addEventListener("orientationchange", fluid_update, false);
        } else {
            window.attachEvent("onresize", fluid_update)
        }
        preempted || app.run();
    });

    if (typeof define === "function" && define.amd) {
        define("Holder", [], function () {
            return app;
        });
    }

})(Holder, window);

//
///*
// HTML5 Shiv v3.6.2 | @afarkas @jdalton @jon_neal @rem | MIT/GPL2 Licensed
// */
//(function(l,f){function m(){var a=e.elements;return"string"==typeof a?a.split(" "):a}function i(a){var b=n[a[o]];b||(b={},h++,a[o]=h,n[h]=b);return b}function p(a,b,c){b||(b=f);if(g)return b.createElement(a);c||(c=i(b));b=c.cache[a]?c.cache[a].cloneNode():r.test(a)?(c.cache[a]=c.createElem(a)).cloneNode():c.createElem(a);return b.canHaveChildren&&!s.test(a)?c.frag.appendChild(b):b}function t(a,b){if(!b.cache)b.cache={},b.createElem=a.createElement,b.createFrag=a.createDocumentFragment,b.frag=b.createFrag();
//    a.createElement=function(c){return!e.shivMethods?b.createElem(c):p(c,a,b)};a.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+m().join().replace(/\w+/g,function(a){b.createElem(a);b.frag.createElement(a);return'c("'+a+'")'})+");return n}")(e,b.frag)}function q(a){a||(a=f);var b=i(a);if(e.shivCSS&&!j&&!b.hasCSS){var c,d=a;c=d.createElement("p");d=d.getElementsByTagName("head")[0]||d.documentElement;c.innerHTML="x<style>article,aside,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}</style>";
//    c=d.insertBefore(c.lastChild,d.firstChild);b.hasCSS=!!c}g||t(a,b);return a}var k=l.html5||{},s=/^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,r=/^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,j,o="_html5shiv",h=0,n={},g;(function(){try{var a=f.createElement("a");a.innerHTML="<xyz></xyz>";j="hidden"in a;var b;if(!(b=1==a.childNodes.length)){f.createElement("a");var c=f.createDocumentFragment();b="undefined"==typeof c.cloneNode||
//    "undefined"==typeof c.createDocumentFragment||"undefined"==typeof c.createElement}g=b}catch(d){g=j=!0}})();var e={elements:k.elements||"abbr article aside audio bdi canvas data datalist details figcaption figure footer header hgroup main mark meter nav output progress section summary time video",version:"3.6.2",shivCSS:!1!==k.shivCSS,supportsUnknownElements:g,shivMethods:!1!==k.shivMethods,type:"default",shivDocument:q,createElement:p,createDocumentFragment:function(a,b){a||(a=f);if(g)return a.createDocumentFragment();
//    for(var b=b||i(a),c=b.frag.cloneNode(),d=0,e=m(),h=e.length;d<h;d++)c.createElement(e[d]);return c}};l.html5=e;q(f)})(this,document);

!function(a){var b=function(){var b=this,c=0,d=!1,e=0,f=0,g=!1;b.settings={block_text:!0,inner_width:!1,theme:"light",flick_animation:"transition-slide",auto_flick:!0,auto_flick_delay:10,dot_navigation:!0,dot_alignment:"center",arrows:!0},b.init=function(f,h){b.settings=a.extend(b.settings,h),d=f,d.addClass("flickerplate"),d.find("ul:first").addClass("flicks"),d.find("li:first").addClass("first-flick"),d.attr("data-flick-position",c);var i=d.data("flick-animation");i&&i.length>0?("transform-slide"==i?b.settings.flick_animation="transform-slide":"transition-slide"==i?b.settings.flick_animation="transition-slide":"jquery-slide"==i?b.settings.flick_animation="jquery-slide":"scroller-slide"==i&&(b.settings.flick_animation="scroller-slide"),d.addClass("animate-"+i)):d.addClass("animate-"+b.settings.flick_animation);var j=d.data("theme"),k=d.find(".first-flick").data("theme");j&&j.length>0?(b.settings.theme=j,d.addClass(k&&k.length>0?"flicker-theme-"+k:"flicker-theme-"+j)):d.addClass("flicker-theme-"+b.settings.theme);var l=d.data("block-text");if(void 0!=l&&0==l&&(b.settings.block_text=!1),d.find("li").each(function(){e++,a(this).wrapInner('<div class="flick-inner"><div class="flick-content"></div></div>'),$flick_block_text=a(this).data("block-text"),void 0!=$flick_block_text?1==$flick_block_text&&(a(this).find(".flick-title").wrapInner('<span class="flick-block-text"></span>'),a(this).find(".flick-sub-text").wrapInner('<span class="flick-block-text"></span>')):1==b.settings.block_text&&(a(this).find(".flick-title").wrapInner('<span class="flick-block-text"></span>'),a(this).find(".flick-sub-text").wrapInner('<span class="flick-block-text"></span>'));var c=a(this).data("background");c&&c.length>0&&a(this).css("background-image","url("+c+")"),$flick_theme=a(this).data("theme"),$flick_theme&&$flick_theme.length>0&&a(this).addClass("flick-theme-"+$flick_theme)}),"scroller-slide"!=b.settings.flick_animation){$data_arrow_navigation=d.data("arrows"),void 0!=$data_arrow_navigation?0!=$data_arrow_navigation&&b.create_arrow_navigation():1==b.settings.arrows&&b.create_arrow_navigation(),$data_dot_navigation=d.data("dot-navigation"),$data_dot_alignment=d.data("dot-alignment");var m=b.settings.dot_alignment;void 0!=$data_dot_alignment&&("left"==$data_dot_alignment?m="left":"right"==$data_dot_alignment&&(m="right")),void 0!=$data_dot_navigation?0!=$data_dot_navigation&&b.create_dot_navigation(m):1==b.settings.dot_navigation&&b.create_dot_navigation(m),$flick_delay=1e3*b.settings.auto_flick_delay,$data_auto_flick=d.data("auto-flick"),$data_auto_flick_delay=d.data("auto-flick-delay"),$data_auto_flick_delay&&($flick_delay=1e3*$data_auto_flick_delay),void 0!=$data_auto_flick&&(b.settings.auto_flick=0!=$data_auto_flick?!0:!1),b.auto_flick_start(),b.flick_flicker(),"jquery-slide"!=b.settings.flick_animation&&d.find("ul.flicks").bind("transitionend MSTransitionEnd webkitTransitionEnd oTransitionEnd",function(){g=!1})}},b.flick_flicker=function(){Modernizr.touch&&d.on("drag",function(a){0==g&&"horizontal"==a.orientation&&(a.preventDefault(),1==a.direction?(c--,0>c?c=0:(g=!0,b.move_flicker(c))):(c++,c==e?c=e-1:(g=!0,b.move_flicker(c))))})},b.create_arrow_navigation=function(){$arrow_nav_html='<div class="arrow-navigation left"><div class="arrow"></div></div>',$arrow_nav_html+='<div class="arrow-navigation right"><div class="arrow"></div></div>',d.prepend($arrow_nav_html),a(".arrow-navigation").mouseover(function(){0==a(this).hasClass("hover")&&a(this).addClass("hover")}),a(".arrow-navigation").mouseout(function(){1==a(this).hasClass("hover")&&a(this).removeClass("hover")}),a(".arrow-navigation").on("click",function(){a(this).hasClass("right")?(c++,c==e&&(c=0)):(c--,0>c&&(c=e-1)),b.move_flicker(c)})},b.create_dot_navigation=function(c){for($dot_nav_html='<div class="dot-navigation '+c+'"><ul>';e>f;)f++,$dot_nav_html+=1==f?'<li><div class="dot active"></div></li>':'<li><div class="dot"></div></li>';$dot_nav_html+="</ul></div>",d.prepend($dot_nav_html),d.find(".dot-navigation li").on("click",function(){b.move_flicker(a(this).index())})},b.auto_flick_start=function(){1==b.settings.auto_flick&&(b.flicker_auto=setInterval(b.auto_flick,$flick_delay))},b.auto_flick=function(){c++,c==e&&(c=0),b.move_flicker(c)},b.auto_flick_stop=function(){b.flicker_auto=clearInterval(b.flicker_auto)},b.auto_flick_reset=function(){b.auto_flick_stop(),b.auto_flick_start()},b.move_flicker=function(a){c=a,"transform-slide"==b.settings.flick_animation?d.find("ul.flicks").attr({style:"-webkit-transform:translate3d(-"+c+"%, 0, 0);-o-transform:translate3d(-"+c+"%, 0, 0);-moz-transform:translate3d(-"+c+"%, 0, 0);transform:translate3d(-"+c+"%, 0, 0)"}):"transition-slide"==b.settings.flick_animation?d.find("ul.flicks").attr({style:"left:-"+c+"00%;"}):"jquery-slide"==b.settings.flick_animation&&d.find("ul.flicks").animate({left:"-"+c+"00%"},function(){g=!1}),$crt_flick=d.find("ul.flicks li:eq("+c+")"),d.removeClass("flicker-theme-light").removeClass("flicker-theme-dark"),d.addClass($crt_flick.hasClass("flick-theme-dark")?"flicker-theme-dark":$crt_flick.hasClass("flick-theme-light")?"flicker-theme-light":"flicker-theme-"+b.settings.theme),d.find(".dot-navigation .dot.active").removeClass("active"),d.find(".dot:eq("+c+")").addClass("active"),d.attr("data-flick-position",c),b.auto_flick_reset()}};a.fn.flicker=function(c){var d=this.length;return this.each(function(e){var f=a(this),g="flickerplate"+(d>1?"-"+ ++e:""),h=(new b).init(f,c);f.data(g,h).data("key",g)})}}(jQuery);
!function(a){function b(a,b){return(m?b.originalEvent.touches[0]:b)["page"+a.toUpperCase()]}function c(b,c,d){var g=a.Event(c,t);a.event.trigger(g,{originalEvent:b},b.target),g.isDefaultPrevented()&&b.preventDefault(),d&&(a.event.remove(r,p+"."+q,e),a.event.remove(r,o+"."+q,f))}function d(d){var k=d.timeStamp||+new Date;i!=k&&(i=k,s.x=t.x=b("x",d),s.y=t.y=b("y",d),s.time=k,s.target=d.target,t.orientation=null,g=!1,h=!1,j=setTimeout(function(){h=!0,c(d,"press")},a.Finger.pressDuration),a.event.add(r,p+"."+q,e),a.event.add(r,o+"."+q,f),u.preventDefault&&d.preventDefault())}function e(d){return t.x=b("x",d),t.y=b("y",d),t.dx=t.x-s.x,t.dy=t.y-s.y,t.adx=Math.abs(t.dx),t.ady=Math.abs(t.dy),(g=t.adx>u.motionThreshold||t.ady>u.motionThreshold)?(clearTimeout(j),t.orientation||(t.adx>t.ady?(t.orientation="horizontal",t.direction=t.dx>0?1:-1):(t.orientation="vertical",t.direction=t.dy>0?1:-1)),d.target!==s.target?(d.target=s.target,void f.call(this,a.Event(o+"."+q,d))):void c(d,"drag")):void 0}function f(a){var b,d=a.timeStamp||+new Date,e=d-s.time;if(clearTimeout(j),a.target===s.target){if(g||h)e<u.flickDuration&&c(a,"flick"),t.end=!0,b="drag";else{var f=k===a.target&&d-l<u.doubleTapInterval;b=f?"doubletap":"tap",k=f?null:s.target,l=d}c(a,b,!0)}}var g,h,i,j,k,l,m="ontouchstart"in window,n=m?"touchstart":"mousedown",o=m?"touchend touchcancel":"mouseup mouseleave",p=m?"touchmove":"mousemove",q="finger",r=a("html")[0],s={},t={},u=a.Finger={pressDuration:300,doubleTapInterval:300,flickDuration:150,motionThreshold:5};a.event.add(r,n+"."+q,d)}(jQuery),function(a){var b=function(){var b=this,c=0,d=!1,e=0,f=0,g=!1;b.settings={block_text:!0,inner_width:!1,theme:"light",flick_animation:"transition-slide",auto_flick:!0,auto_flick_delay:10,dot_navigation:!0,dot_alignment:"center",arrows:!0},b.init=function(f,h){b.settings=a.extend(b.settings,h),d=f,d.addClass("flickerplate"),d.find("ul:first").addClass("flicks"),d.find("li:first").addClass("first-flick"),d.attr("data-flick-position",c);var i=d.data("flick-animation");i&&i.length>0?("transform-slide"==i?b.settings.flick_animation="transform-slide":"transition-slide"==i?b.settings.flick_animation="transition-slide":"jquery-slide"==i?b.settings.flick_animation="jquery-slide":"scroller-slide"==i&&(b.settings.flick_animation="scroller-slide"),d.addClass("animate-"+i)):d.addClass("animate-"+b.settings.flick_animation);var j=d.data("theme"),k=d.find(".first-flick").data("theme");j&&j.length>0?(b.settings.theme=j,d.addClass(k&&k.length>0?"flicker-theme-"+k:"flicker-theme-"+j)):d.addClass("flicker-theme-"+b.settings.theme);var l=d.data("block-text");if(void 0!=l&&0==l&&(b.settings.block_text=!1),d.find("li").each(function(){e++,a(this).wrapInner('<div class="flick-inner"><div class="flick-content"></div></div>'),$flick_block_text=a(this).data("block-text"),void 0!=$flick_block_text?1==$flick_block_text&&(a(this).find(".flick-title").wrapInner('<span class="flick-block-text"></span>'),a(this).find(".flick-sub-text").wrapInner('<span class="flick-block-text"></span>')):1==b.settings.block_text&&(a(this).find(".flick-title").wrapInner('<span class="flick-block-text"></span>'),a(this).find(".flick-sub-text").wrapInner('<span class="flick-block-text"></span>'));var c=a(this).data("background");c&&c.length>0&&a(this).css("background-image","url("+c+")"),$flick_theme=a(this).data("theme"),$flick_theme&&$flick_theme.length>0&&a(this).addClass("flick-theme-"+$flick_theme)}),"scroller-slide"!=b.settings.flick_animation){$data_arrow_navigation=d.data("arrows"),void 0!=$data_arrow_navigation?0!=$data_arrow_navigation&&b.create_arrow_navigation():1==b.settings.arrows&&b.create_arrow_navigation(),$data_dot_navigation=d.data("dot-navigation"),$data_dot_alignment=d.data("dot-alignment");var m=b.settings.dot_alignment;void 0!=$data_dot_alignment&&("left"==$data_dot_alignment?m="left":"right"==$data_dot_alignment&&(m="right")),void 0!=$data_dot_navigation?0!=$data_dot_navigation&&b.create_dot_navigation(m):1==b.settings.dot_navigation&&b.create_dot_navigation(m),$flick_delay=1e3*b.settings.auto_flick_delay,$data_auto_flick=d.data("auto-flick"),$data_auto_flick_delay=d.data("auto-flick-delay"),$data_auto_flick_delay&&($flick_delay=1e3*$data_auto_flick_delay),void 0!=$data_auto_flick&&(b.settings.auto_flick=0!=$data_auto_flick?!0:!1),b.auto_flick_start(),b.flick_flicker(),"jquery-slide"!=b.settings.flick_animation&&d.find("ul.flicks").bind("transitionend MSTransitionEnd webkitTransitionEnd oTransitionEnd",function(){g=!1})}},b.flick_flicker=function(){Modernizr.touch&&d.on("drag",function(a){0==g&&"horizontal"==a.orientation&&(a.preventDefault(),1==a.direction?(c--,0>c?c=0:(g=!0,b.move_flicker(c))):(c++,c==e?c=e-1:(g=!0,b.move_flicker(c))))})},b.create_arrow_navigation=function(){$arrow_nav_html='<div class="arrow-navigation left"><div class="arrow"></div></div>',$arrow_nav_html+='<div class="arrow-navigation right"><div class="arrow"></div></div>',d.prepend($arrow_nav_html),a(".arrow-navigation").mouseover(function(){0==a(this).hasClass("hover")&&a(this).addClass("hover")}),a(".arrow-navigation").mouseout(function(){1==a(this).hasClass("hover")&&a(this).removeClass("hover")}),a(".arrow-navigation").on("click",function(){a(this).hasClass("right")?(c++,c==e&&(c=0)):(c--,0>c&&(c=e-1)),b.move_flicker(c)})},b.create_dot_navigation=function(c){for($dot_nav_html='<div class="dot-navigation '+c+'"><ul>';e>f;)f++,$dot_nav_html+=1==f?'<li><div class="dot active"></div></li>':'<li><div class="dot"></div></li>';$dot_nav_html+="</ul></div>",d.prepend($dot_nav_html),d.find(".dot-navigation li").on("click",function(){b.move_flicker(a(this).index())})},b.auto_flick_start=function(){1==b.settings.auto_flick&&(b.flicker_auto=setInterval(b.auto_flick,$flick_delay))},b.auto_flick=function(){c++,c==e&&(c=0),b.move_flicker(c)},b.auto_flick_stop=function(){b.flicker_auto=clearInterval(b.flicker_auto)},b.auto_flick_reset=function(){b.auto_flick_stop(),b.auto_flick_start()},b.move_flicker=function(a){c=a,"transform-slide"==b.settings.flick_animation?d.find("ul.flicks").attr({style:"-webkit-transform:translate3d(-"+c+"%, 0, 0);-o-transform:translate3d(-"+c+"%, 0, 0);-moz-transform:translate3d(-"+c+"%, 0, 0);transform:translate3d(-"+c+"%, 0, 0)"}):"transition-slide"==b.settings.flick_animation?d.find("ul.flicks").attr({style:"left:-"+c+"00%;"}):"jquery-slide"==b.settings.flick_animation&&d.find("ul.flicks").animate({left:"-"+c+"00%"},function(){g=!1}),$crt_flick=d.find("ul.flicks li:eq("+c+")"),d.removeClass("flicker-theme-light").removeClass("flicker-theme-dark"),d.addClass($crt_flick.hasClass("flick-theme-dark")?"flicker-theme-dark":$crt_flick.hasClass("flick-theme-light")?"flicker-theme-light":"flicker-theme-"+b.settings.theme),d.find(".dot-navigation .dot.active").removeClass("active"),d.find(".dot:eq("+c+")").addClass("active"),d.attr("data-flick-position",c),b.auto_flick_reset()}};a.fn.flicker=function(c){var d=this.length;return this.each(function(e){var f=a(this),g="flickerplate"+(d>1?"-"+ ++e:""),h=(new b).init(f,c);f.data(g,h).data("key",g)})}}(jQuery);
!function(e){var t={sectionContainer:"section",easing:"ease",animationTime:1e3,pagination:true,updateURL:false,keyboard:true,beforeMove:null,afterMove:null,loop:false,responsiveFallback:false};e.fn.swipeEvents=function(){return this.each(function(){function i(e){var i=e.originalEvent.touches;if(i&&i.length){t=i[0].pageX;n=i[0].pageY;r.bind("touchmove",s)}}function s(e){var i=e.originalEvent.touches;if(i&&i.length){var o=t-i[0].pageX;var u=n-i[0].pageY;if(o>=50){r.trigger("swipeLeft")}if(o<=-50){r.trigger("swipeRight")}if(u>=50){r.trigger("swipeUp")}if(u<=-50){r.trigger("swipeDown")}if(Math.abs(o)>=50||Math.abs(u)>=50){r.unbind("touchmove",s)}}}var t,n,r=e(this);r.bind("touchstart",i)})};e.fn.onepage_scroll=function(n){function o(){if(e(window).width()<r.responsiveFallback){e("body").addClass("disabled-onepage-scroll");e(document).unbind("mousewheel DOMMouseScroll");i.swipeEvents().unbind("swipeDown swipeUp")}else{if(e("body").hasClass("disabled-onepage-scroll")){e("body").removeClass("disabled-onepage-scroll");e("html, body, .wrapper").animate({scrollTop:0},"fast")}i.swipeEvents().bind("swipeDown",function(t){if(!e("body").hasClass("disabled-onepage-scroll"))t.preventDefault();i.moveUp()}).bind("swipeUp",function(t){if(!e("body").hasClass("disabled-onepage-scroll"))t.preventDefault();i.moveDown()});e(document).bind("mousewheel DOMMouseScroll",function(e){e.preventDefault();var t=e.originalEvent.wheelDelta||-e.originalEvent.detail;u(e,t)})}}function u(e,t){var n=t,s=(new Date).getTime();if(s-lastAnimation<quietPeriod+r.animationTime){e.preventDefault();return}if(n<0){i.moveDown()}else{i.moveUp()}lastAnimation=s}var r=e.extend({},t,n),i=e(this),s=e(r.sectionContainer);total=s.length,status="off",topPos=0,lastAnimation=0,quietPeriod=500,paginationList="";e.fn.transformPage=function(t,n,r){if(typeof t.beforeMove=="function")t.beforeMove(r);e(this).css({"-webkit-transform":"translate3d(0, "+n+"%, 0)","-webkit-transition":"-webkit-transform "+t.animationTime+"ms "+t.easing,"-moz-transform":"translate3d(0, "+n+"%, 0)","-moz-transition":"-moz-transform "+t.animationTime+"ms "+t.easing,"-ms-transform":"translate3d(0, "+n+"%, 0)","-ms-transition":"-ms-transform "+t.animationTime+"ms "+t.easing,transform:"translate3d(0, "+n+"%, 0)",transition:"transform "+t.animationTime+"ms "+t.easing});e(this).one("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend",function(e){if(typeof t.afterMove=="function")t.afterMove(r)})};e.fn.moveDown=function(){var t=e(this);index=e(r.sectionContainer+".active").data("index");current=e(r.sectionContainer+"[data-index='"+index+"']");next=e(r.sectionContainer+"[data-index='"+(index+1)+"']");if(next.length<1){if(r.loop==true){pos=0;next=e(r.sectionContainer+"[data-index='1']")}else{return}}else{pos=index*100*-1}if(typeof r.beforeMove=="function")r.beforeMove(next.data("index"));current.removeClass("active");next.addClass("active");if(r.pagination==true){e(".onepage-pagination li a"+"[data-index='"+index+"']").removeClass("active");e(".onepage-pagination li a"+"[data-index='"+next.data("index")+"']").addClass("active")}e("body")[0].className=e("body")[0].className.replace(/\bviewing-page-\d.*?\b/g,"");e("body").addClass("viewing-page-"+next.data("index"));if(history.replaceState&&r.updateURL==true){var n=window.location.href.substr(0,window.location.href.indexOf("#"))+"#"+(index+1);history.pushState({},document.title,n)}t.transformPage(r,pos,next.data("index"))};e.fn.moveUp=function(){var t=e(this);index=e(r.sectionContainer+".active").data("index");current=e(r.sectionContainer+"[data-index='"+index+"']");next=e(r.sectionContainer+"[data-index='"+(index-1)+"']");if(next.length<1){if(r.loop==true){pos=(total-1)*100*-1;next=e(r.sectionContainer+"[data-index='"+total+"']")}else{return}}else{pos=(next.data("index")-1)*100*-1}if(typeof r.beforeMove=="function")r.beforeMove(next.data("index"));current.removeClass("active");next.addClass("active");if(r.pagination==true){e(".onepage-pagination li a"+"[data-index='"+index+"']").removeClass("active");e(".onepage-pagination li a"+"[data-index='"+next.data("index")+"']").addClass("active")}e("body")[0].className=e("body")[0].className.replace(/\bviewing-page-\d.*?\b/g,"");e("body").addClass("viewing-page-"+next.data("index"));if(history.replaceState&&r.updateURL==true){var n=window.location.href.substr(0,window.location.href.indexOf("#"))+"#"+(index-1);history.pushState({},document.title,n)}t.transformPage(r,pos,next.data("index"))};e.fn.moveTo=function(t){current=e(r.sectionContainer+".active");next=e(r.sectionContainer+"[data-index='"+t+"']");if(next.length>0){if(typeof r.beforeMove=="function")r.beforeMove(next.data("index"));current.removeClass("active");next.addClass("active");e(".onepage-pagination li a"+".active").removeClass("active");e(".onepage-pagination li a"+"[data-index='"+t+"']").addClass("active");e("body")[0].className=e("body")[0].className.replace(/\bviewing-page-\d.*?\b/g,"");e("body").addClass("viewing-page-"+next.data("index"));pos=(t-1)*100*-1;if(history.replaceState&&r.updateURL==true){var n=window.location.href.substr(0,window.location.href.indexOf("#"))+"#"+(t-1);history.pushState({},document.title,n)}i.transformPage(r,pos,t)}};i.addClass("onepage-wrapper").css("position","relative");e.each(s,function(t){e(this).addClass("ops-section").attr("data-index",t+1);topPos=topPos+100;if(r.pagination==true){paginationList+="<li><a data-index='"+(t+1)+"' href='#"+(t+1)+"'></a></li>"}});i.swipeEvents().bind("swipeDown",function(t){if(!e("body").hasClass("disabled-onepage-scroll"))t.preventDefault();i.moveUp()}).bind("swipeUp",function(t){if(!e("body").hasClass("disabled-onepage-scroll"))t.preventDefault();i.moveDown()});if(r.pagination==true){e("<ul class='onepage-pagination'>"+paginationList+"</ul>").prependTo("body");posTop=i.find(".onepage-pagination").height()/2*-1;i.find(".onepage-pagination").css("margin-top",posTop)}if(window.location.hash!=""&&window.location.hash!="#1"){init_index=window.location.hash.replace("#","");e(r.sectionContainer+"[data-index='"+init_index+"']").addClass("active");e("body").addClass("viewing-page-"+init_index);if(r.pagination==true)e(".onepage-pagination li a"+"[data-index='"+init_index+"']").addClass("active");next=e(r.sectionContainer+"[data-index='"+init_index+"']");if(next){next.addClass("active");if(r.pagination==true)e(".onepage-pagination li a"+"[data-index='"+init_index+"']").addClass("active");e("body")[0].className=e("body")[0].className.replace(/\bviewing-page-\d.*?\b/g,"");e("body").addClass("viewing-page-"+next.data("index"));if(history.replaceState&&r.updateURL==true){var a=window.location.href.substr(0,window.location.href.indexOf("#"))+"#"+init_index;history.pushState({},document.title,a)}}pos=(init_index-1)*100*-1;i.transformPage(r,pos,init_index)}else{e(r.sectionContainer+"[data-index='1']").addClass("active");e("body").addClass("viewing-page-1");if(r.pagination==true)e(".onepage-pagination li a"+"[data-index='1']").addClass("active")}if(r.pagination==true){e(".onepage-pagination li a").click(function(){var t=e(this).data("index");i.moveTo(t)})}e(document).bind("mousewheel DOMMouseScroll",function(t){t.preventDefault();var n=t.originalEvent.wheelDelta||-t.originalEvent.detail;if(!e("body").hasClass("disabled-onepage-scroll"))u(t,n)});if(r.responsiveFallback!=false){e(window).resize(function(){o()});o()}if(r.keyboard==true){e(document).keydown(function(t){var n=t.target.tagName.toLowerCase();if(!e("body").hasClass("disabled-onepage-scroll")){switch(t.which){case 38:if(n!="input"&&n!="textarea")i.moveUp();break;case 40:if(n!="input"&&n!="textarea")i.moveDown();break;default:return}}})}return false}}(window.jQuery);