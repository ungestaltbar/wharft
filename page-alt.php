<?php
/*
 * Template Name: Page Alt.
 */
get_header(); ?>

<?php do_action('area', 'header-primary', null); ?>

    <div class="content-wrapper row">

        <?php do_action('area', 'content-primary', null, array('context' => 'content', 'mergeRepeating' => true)); ?>
        <?php do_action('area', 'content-primary-2', null, array('context' => 'content', 'mergeRepeating' => true)); ?>


        <!--        <div class="large-4 columns">-->
        <!--            --><?php //do_action('sidebar_areas', null, array('context' => 'side')); ?>
        <!--        </div>-->
    </div>


<?php get_footer(); ?>