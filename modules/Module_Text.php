<?php

use Kontentblocks\Modules\Module,
    Kontentblocks\Templating\ModuleTemplate;


class Module_Text extends Module
{

    public static $defaults = array(
        'publicName' => 'WYSIWYG',
        'name' => 'Text Editor',
        'description' => 'Some short description',
        'globallyAvailable' => true,
        'asTemplate' => true,
        'connect' => array('normal', 'side'),
        'id' => 'wysiwyg',
        'controls' => array(
            'width' => 600
        )
    );

    public function render($data)
    {
        $folder = trailingslashit($this->getEnvVar('area_template'));
        $tpl = 'normal.twig';


        $tpl = new ModuleTemplate($this, 'wysiwyg/' .$folder . $tpl);
        return $tpl->render();

    }

    public function fields()
    {

        $groupB = $this->Fields->addGroup('Second', array('label' => 'Options'))
            ->addField(
                'editor', 'sometext', array(
                'label' => 'Sometext',
                'areaContext' => array('normal', 'side'),
                'media' => true,
                'returnObj' => 'Element',
                'concat' => true
            ));
        $groupC = $this->Fields->addGroup('Third', array('label' => 'Layout'))
            ->addField(
                'color', 'background', array(
                'label' => 'Background Color',
                'description' => '',
                'returnObj' => false,
                'std' => '#000',
                'concat' => false
            ));

    }

}
