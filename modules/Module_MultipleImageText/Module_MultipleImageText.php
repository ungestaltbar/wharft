<?php
use Kontentblocks\Backend\Environment\Save\ConcatContent;
use Kontentblocks\Modules\Module,
    Kontentblocks\Templating\ModuleTemplate;
use Kontentblocks\Utils\JSONBridge;

class Module_MultipleImageText extends Module
{
    // Module default settings
    public static $defaults = array(
        'publicName' => 'Slider / Features',
        'name' => 'Slider / Features',
        'description' => 'This module allows you to add multiple image/text items.',
        'globallyAvailable' => true,
        'asTemplate' => true,
        'connect' => 'any',
        'id' => 'multiple-image-text',
        'controls' => array(
            'width' => 800
        ),
        'category' => 'standard'
    );


    public static function init($args)
    {
        add_filter('kb_modify_module_data', array(__CLASS__, 'modifyModuleData'), 10, 2);
        \Kontentblocks\Hooks\Enqueues::getInstance()->addScript(array(
            'handle' => 'module-mit',
            'src' => $args['settings']['uri'] . '/module-mit.js',
            'deps' => array('underscore', 'backbone')
        ));

    }


    public function optionsCallback($data)
    {
        $data = $this->prepareData($data);
        $Bridge = JSONBridge::getInstance();
        $Bridge->registerData('moduleData', $this->instance_id, $data);
        $tpl = new ModuleTemplate($this, 'templates/backend/options.twig');
        $tpl->setPath($this->settings['path']);
        $tpl->render(true);
    }

    public function render($data)
    {
        // expose module data to user facing side and make it available to js
        $Bridge = JSONBridge::getInstance();
        $Bridge->registerData('moduleData', $this->instance_id, $data);

        if (empty($data['items']))
            return;

        $coll = array();
        foreach ($data['items'] as $k => $s) {
            if (!is_array($s)) {
                continue;
            }
            $item = array(
                'content' => new \Kontentblocks\Fields\Returnobjects\Element($s['content'], array(
                        'instance_id' => $this->instance_id,
                        'key' => 'content',
                        'arrayKey' => 'items',
                        'index' => $k
                    )),
                'image' => \Kontentblocks\Utils\ImageResize::getInstance()->process($s['imgid'], 1600, 500, true, true, true),
                'circle' => \Kontentblocks\Utils\ImageResize::getInstance()->process($s['imgid'], 400, 400, true, true, true)
            );
            $coll[] = $item;
        }

        // additional twig template data
        $tplData = array(
            'items' => $coll
        );
        $tpl = new ModuleTemplate($this, 'templates/frontend/' . $this->getData('template'), $tplData);
        $tpl->setPath($this->getSetting('path'));

        return $tpl->render();
    }

    public function fields()
    {
        $this->Fields->addGroup('test', array('label' => 'Items'))
            ->addField(
                'callback', 'items', array(
                    'label' => 'Items',
                    'callback' => array($this, 'optionsCallback'),
                    'args' => array($this->moduleData)
                )
            );
        $this->Fields->addGroup('more', array('label' => 'Layout'))
            ->addField(
                'select', 'template', array(
                    'label' => 'Template',
                    'options' => array(
                        array(
                            'name' => 'Slider',
                            'value' => 'normal.twig'
                        ),
                        array(
                            'name' => 'Featurettes',
                            'value' => 'teaser.twig'
                        ),
                        array(
                            'name' => 'Flicker',
                            'value' => 'flickerplate.twig'
                        ),
                    ),
                    'std' => 'normal.twig'
                )
            )
            ->addField(
                'color', 'background', array(
                    'label' => 'Background Color',
                    'std' => '#f3f3f3'
                )
            )->addField(
                'color', 'color', array(
                    'label' => 'Background Color',
                    'std' => '#f3f3f3'
                )
            );
    }

    public function save($data, $old)
    {

//        if (defined('KB_FRONTEND_SAVE') && KB_FRONTEND_SAVE) {
//            return $data;
//        }


        if (!isset($data['items']) || !is_array($data['items']))
            return $data;

        if ($this->isPublic()) {
            foreach ($data['items'] as $item) {

                if ($item['imgid']) {
                    $thumb = wp_get_attachment_image($item['imgid'], 'thumbnail');
                    ConcatContent::getInstance()->addString($thumb);
                }

                ConcatContent::getInstance()->addString($item['content']);
            }
        }


        return stripslashes_deep($data);
    }


    private function prepareData($data)
    {
        if (empty($data['items']) || !is_array($data['items'])) {
            return array();
        }
        $pre = array();
        foreach ($data['items'] as $item) {

            $item['imgsrc'] = (isset($item['imgid'])) ? wp_prepare_attachment_for_js($item['imgid']) : null;
            $pre[] = $item;
        }
        $data['items'] = $pre;
        return $data;
    }

    /**
     * Pre-Output filter
     * Adds in the image complete informations
     * @param $data
     * @param $settings
     * @return array
     */
    public static function modifyModuleData($data, $settings)
    {
        if ($settings['class'] !== 'Module_MultipleImageText') {
            return $data;
        }

        if (empty($data['items'])) {
            return $data;
        }

        $pre = array();
        foreach ($data['items'] as $item) {
            if (!isset($item['imgid'])) {
                continue;
            }
            $item['imgsrc'] = wp_prepare_attachment_for_js($item['imgid']);
            $pre['items'][] = $item;
        }
        $data['items'] = $pre['items'];
        return $data;
    }

}
