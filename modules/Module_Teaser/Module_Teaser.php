<?php

use Kontentblocks\Modules\Module,
    Kontentblocks\Templating\ModuleTemplate;


class Module_Teaser extends Module
{

    public static $defaults = array(
        'publicName' => 'Image/Text',
        'name' => 'Image/Text',
        'description' => '',
        'globallyAvailable' => true,
        'asTemplate' => true,
        'connect' => array('normal', 'side', 'top'),
        'category' => 'media',
        'id' => 'imagetext',
        'controls' => array(
            'width' => 700
        )
    );

    public function render($data)
    {

        $tpl = $this->getData('template', null, 'image-left.twig');

        $folder = $this->getEnvVar('pageTemplate');

        $tpl = new ModuleTemplate($this, trailingslashit($folder) . $tpl);
        $tpl->setPath($this->getPath());
        return $tpl->render();

    }

    public function fields()
    {
        $groupA = $this->Fields->addGroup('First', array('label' => 'Image'))
            ->addField(
                'image', 'image', array(
                'label' => 'Image',
                'description' => '',
                'type' => 'text',
                'returnObj' => 'Image'
            ))
            ->addField(
                'text', 'width', array(
                'label' => 'Image width',
                'description' => 'Width in px',
                'type' => 'text',
                'returnObj' => false,
                'std' => 300
            ))
            ->addField(
                'text', 'height', array(
                'label' => 'Image height',
                'description' => 'Height in px',
                'type' => 'text',
                'returnObj' => false,
                'std' => 500
            ));

        $groupB = $this->Fields->addGroup('Second', array('label' => 'Content'))
            ->addField(
                'editor', 'content', array(
                'label' => 'Content',
                'media' => false,
                'returnObj' => 'Element'
            ));

        $groupC = $this->Fields->addGroup('Third', array('label' => 'Layout'))
            ->addField(
                'color', 'background', array(
                'label' => 'Background Color',
                'description' => '',
                'returnObj' => false,
                'std' => '#000'
            ))
            ->addField(
                'select', 'template', array(
                    'label' => 'Template',
                    'options' => array(
                        array(
                            'name' => 'Image left',
                            'value' => 'image-left.twig'
                        ),
                        array(
                            'name' => 'Image right',
                            'value' => 'image-right.twig'
                        )
                    ),
                    'std' => 'image-left.twig'
                )
            );

    }

}
