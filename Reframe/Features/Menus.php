<?php

namespace Reframe\Features;

class Menus
{

    protected $menus = array( );

    public function __construct()
    {
        add_action( 'init', array( $this, '_addMenus' ) );

    }

    public function add( $id, $name )
    {
        if ( !isset( $this->menus[ $id ] ) ) {
            $this->menus[ $id ] = $name;
        }
        return $this;

    }

    public function _addMenus()
    {
        register_nav_menus( $this->menus );
    }

}