<?php
namespace Reframe\Features;

class ThemeSupport
{
    
    protected $features = array();
    
    public function __construct()
    {
        add_action( 'after_setup_theme', array($this, '_addSupport'));
    }
    
    public function add($feature, $args = array())
    {
        if (!isset($this->features[$feature])){
            $this->features[$feature] = $args;
        }
        return $this;
    }
    
    public function _addSupport()
    {
        foreach ($this->features as $feature => $args)
        {
            add_theme_support($feature, $args);
        }
    }
    
}