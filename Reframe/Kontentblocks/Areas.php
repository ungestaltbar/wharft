<?php

namespace Reframe\Kontentblocks;

class Areas
{

    protected $areas = array( );

    public function add( $args )
    {

        if ( function_exists( 'kb_register_area' ) ) {
            kb_register_area( $args );
        }
        return $this;
    }
    
    public function template()
    {
        
    }

}