<?php

namespace Reframe\Kontentblocks;

class Kontentblocks
{

    protected $paths = array( );
    private $theme;

    public function __construct( $theme )
    {
        if ( !is_a( $theme, 'Reframe\Theme' ) ) {
            throw new \Exception( 'Theme must be of class Theme' );
        }

        $this->theme = $theme;

        add_filter( 'kb_add_module_path', array( $this, '_addPaths' ) );

    }

    public function addPath( $path )
    {
        if (!is_dir( $this->theme->getPath() . trailingslashit( $path))){
            mkdir($this->theme->getPath() . trailingslashit( $path ), 0777, true);
        }
        
        $this->paths[ ] = $this->theme->getPath() . trailingslashit( $path );
    }

    public function _addPaths( $paths )
    {
        return array_merge( $paths, $this->paths );
    }

}