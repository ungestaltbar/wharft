<?php

namespace Reframe;

class Theme
{

    protected $uri;
    protected $path;
    
    public $Scripts;
    public $Styles;

    public $Menus;
    public $ThemeSupport;
    
    public $Areas;
    function __construct( $uri = NULL, $path = null )
    {
        if ( !$uri ) {
            $this->uri = trailingslashit( get_stylesheet_directory_uri() );
        }
        else {
            $this->uri = trailingslashit( $uri );
        }

        if ( !$path ) {
            $this->path = trailingslashit( get_stylesheet_directory() );
        }
        else {
            $this->path = trailingslashit( $path );
        }

        $this->Scripts = new Files\Scripts( $this );
        $this->Styles  = new Files\Styles( $this );
        $this->Menus = new Features\Menus();
        $this->ThemeSupport = new Features\ThemeSupport();
        $this->Areas = new Kontentblocks\Areas();
        $this->Kontentblocks = new Kontentblocks\Kontentblocks($this);
    }

    public function getPath()
    {
        return $this->path;

    }

    public function getUri()
    {
        return $this->uri;

    }

}