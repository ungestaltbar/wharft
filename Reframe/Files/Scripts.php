<?php

namespace Reframe\Files;

class Scripts
{

    protected $theme;
    protected $scripts = array();

    public function __construct( $theme )
    {
        if ( !is_a( $theme, 'Reframe\Theme' ) ) {
            throw new \Exception( 'Theme must be of class Theme' );
        }
        $this->theme = $theme;

        add_action( 'wp_enqueue_scripts', array( $this, '_enqueueScripts' ) );

    }

    public function addHeaderScript( $handle, $src, $deps = null )
    {
//        $ver                      = filemtime( $this->theme->getPath() . $src );
        $this->scripts[ $handle ] = array(
            'handle' => $handle,
            'src' => $src,
            'deps' => $deps,
            'ver' => 'alpha',
            'inFooter' => false );
        return $this;

    }

    public function addFooterScript( $handle, $src, $deps = null )
    {
//        $ver                      = filemtime( $this->theme->getPath() . $src );
        $this->scripts[ $handle ] = array(
            'handle' => $handle,
            'src' => $src,
            'deps' => $deps,
            'ver' => 'alpha',
            'inFooter' => true );
        return $this;

    }

    public function _enqueueScripts()
    {

        foreach ( $this->scripts as $handle => $script ) {
            wp_enqueue_script( $handle, $this->theme->getUri() . $script[ 'src' ], $script[ 'deps' ], $script[ 'ver' ], $script[ 'inFooter' ] );
        }

    }

}
