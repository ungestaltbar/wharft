<?php

namespace Reframe\Files;

class Styles
{

    protected $theme;
    protected $styles = array( );

    public function __construct( $theme )
    {
        if ( !is_a( $theme, 'Reframe\Theme' ) ) {
            throw new \Exception( 'Theme must be of class Theme' );
        }

        $this->theme = $theme;

        add_action( 'wp_enqueue_scripts', array( $this, '_enqueueStyles' ) );

    }

    public function addStyle( $handle, $src, $deps = null )
    {

        $ver                     = 'fake'; //filemtime( $this->theme->getPath() . $src );
        $this->styles[ $handle ] = array(
            'handle' => $handle,
            'src' => $src,
            'deps' => $deps,
            'ver' => $ver
        );
        return $this;

    }

    public function _enqueueStyles()
    {
        foreach ( $this->styles as $handle => $style ) {
            wp_enqueue_style( $handle, $this->theme->getUri() . $style[ 'src' ], $style[ 'deps' ], $style[ 'ver' ] );
        }
    }

}